# Workspace initialization script

## Properties

`DESC`

A short description of what this target is, what it does and what it checks for.

`TITLE` (unimplemented)

A title for this target.

`DEPS`

The dependencies for this target. The dependencies are other targets, meaning that the highest-level targets shouldn't have dependencies.

`PROVIDES`

The additional targets this target provides. This is used in mapping the shortest path between targets for a given recipe. The provided targets should exist inside this target directory.

`ROOT`

If this key is set, the target needs root privileges. If any installation target needs root privileges, the script will require root before installation.
