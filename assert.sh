# ...

assert_argument() {
	if [ -z "$1" ]; then
		echo "Wrong number of arguments given!";
		exit 1;
	fi
}

assert_equals() {
	got=$1;
	want=$2;
	[ "$got" == "$want" ];
	EC=$?;
	if [ $EC -gt 0 ]; then
		echo "Assert equals failed:";
		echo "Want: '$want'";
		echo "Got : '$got'";
	fi
	return $EC;
}

assert_exec_equals() {
	exec=$1;
	shift;
	assert_equals "$($exec 2>&1)" "$@";
}

assert_exec() {
	exec=$1;
	want=$2;
	[ -z "$want" ] && want=0;
	LOG="$($exec)";
	got=$?;
	[ "$got" == "$want" ];
	EC=$?;
	if [ $EC -gt 0 ]; then
		echo "Assert exec failed:";
		echo "Return code: '$got'";
		echo "Want       : '$want'";
		echo "Log        :";
		echo "$LOG";
	fi
	return $EC;
}

assert_exec_find() {
	exec=$1;
	shift;
	assert_find "$($exec 2>&1)" "$@";
}

assert_exec_find_neg() {
	exec=$1;
	shift;
	assert_find_neg "$($exec 2>&1)" "$@";
}

assert_fail() {
	exec="$@";
	LOG="$($exec)";
	EC=$?;
	if [ ! $EC -gt 0 ]; then
		echo "Assert fail failed:";
		echo "$@ succeeded with code $EC";
		echo "Log:";
		echo "$LOG";
		return 1;
	fi
	return 0;
}

assert_file() {
	for file in $@; do
		if [ ! -f "$file" ]; then
			echo "File assertion failed:";
			echo "$(realpath $file) does not exist.";
			return 1;
		fi
	done
}

assert_file_neg() {
	for file in $@; do
		if [ -f "$file" ]; then
			echo "File negative assertion failed:";
			echo "$(realpath $file) exists, even if it shouldn't.";
			return 1;
		fi
	done
}

assert_find() {
	haystack=$1;
	shift;
	for needle in $@; do
		echo "$haystack" | grep -q "$needle";
		EC=$?;
		if [ $EC -gt 0 ]; then
			echo "Assert find failed:";
			echo "Haystack: '$haystack'";
			echo "Needle  : '$needle'";
			return $EC;
		fi
	done
	return $EC;
}

assert_find_neg() {
	haystack=$1;
	shift;
	for needle in $@; do
		echo "$haystack" | grep -q "$needle";
		EC=$?;
		if [ $EC = 0 ]; then
			echo "Assert negative find failed:";
			echo "Haystack: '$haystack'";
			echo "Needle  : '$needle'";
			return 1;
		fi
	done
	return 0;
}

assert_folder() {
	for folder in $@; do
		if [ ! -d "$folder" ]; then
			echo "Folder assertion failed:";
			echo "$(realpath $folder) does not exist.";
			return 1;
		fi
	done
}

assert_success() {
	exec="$@";
	LOG="$($exec)";
	EC=$?;
	if [ $EC -gt 0 ]; then
		echo "Assert success failed:";
		echo "$@ failed with code $EC";
		echo "Log:";
		echo "$LOG";
		return 1;
	fi
	return 0;
}
