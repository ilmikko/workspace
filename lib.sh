[ -z "$DIR" ] && DIR=$(dirname $0);

import() {
	for lib in $@; do
		. "$lib" || exit 5;
	done
}

import \
	"$DIR/assert.sh" \
	"$DIR/lib/helpers.sh" \
	"$DIR/lib/logging.sh" \
	"$DIR/lib/input.sh" \
	"$DIR/lib/parse.sh" \
	"$DIR/lib/script.sh" \
	"$DIR/lib/script_references.sh" \
	"$DIR/lib/user.sh" \
	"$DIR/lib/check.sh" \
	"$DIR/lib/install.sh" \
	"$DIR/lib/package.sh" \
	"$DIR/lib/recipe.sh" \
	"$DIR/lib/user.sh" \
	"$DIR/lib/variable.sh";

usage() {
	echo "Usage: $0 [target]";
}

# Process files based on their extension.
process() {
	for file in $@; do
		file=$(find_file $file);
		[ $? -gt 0 ] && fail "File not found: $file";
		case $file in
			*.check)
				check $file || exit 1;
				;;
			*.install)
				install $file || exit 1;
				;;
			*.recipe)
				recipe $file || exit 1;
				;;
			*.package)
				package_display $file || exit 1;
				;;
			*)
				fail "Unknown file: $file";
				;;
		esac
	done
}

# STARTHELP

args_global() {
	args="";
	while [ $# -gt 0 ]; do
		debug "Global argument: $1";
		case $1 in
			--interactive) # Run in interactive mode. (default)
				INTERACTIVE=1;
				;;
			--interactive-off) # Do not run in interactive mode, assume there is no user input.
				INTERACTIVE=0;
				;;
			--silent) # Only output errors and output from packages.
				increase_silence;
				;;
			--verbose) # Enables more verbose logging.
				increase_verbosity;
				;;
			*=*) # Sets a variable to a value. For example, UID=1000 would set the UID variable to 1000.
				set_variable $(echo $1 | cut -d= -f1) $(echo $1 | cut -d= -f2-);
				;;
			*)
				args="$args $1";
				;;
		esac
		shift;
	done
}

args_common() {
	args="";
	while [ $# -gt 0 ]; do
		debug "Common argument: $1";
		case $1 in
			--help) # Prints out this help text.
				usage;
				log;
				log "Commands:";
				# Get this case statement.
				statement=$(cat $DIR/lib.sh | awk '/STARTHEL\P/ { help=1; } /ENDHEL\P/ { help=0; } { if (help) print $0 }');
				echo "$statement" | awk '/^\s*.*)\s*#.*/ { gsub(/)$/,"",$1); sub(/#/,"\t",$0); cmd=$1; $1=""; printf "%-12s%s\n",cmd,$0; }';
				;;
			--install) # Installs a package.
				shift;
				assert_argument $1 || exit 1;
				file=$(find_file $1);
				[ $? -gt 0 ] && fail "File not found: $file";
				# Make sure the package has all the variables that are needed.
				fill_variables $file;
				package $file || exit 1;
				;;
			*=*)
				# Skip over variables as they are parsed elsewhere.
				;;
			--verbose)
				# Skip over as this is parsed above.
				;;
			--*)
				error "Unknown command: $1!";
				exit 2;
				;;
			*)
				args="$args $1";
				;;
		esac
		shift;
	done
}

# ENDHELP

main() {
	# Try to figure out what we want to do with each of the arguments.
	args="$@";
	# We seek some 'global' arguments first, as they apply to all of the commands.
	args_global $args;
	# Then the rest.
	args_common $args;

	if [ ! -z "$args" ]; then
		# Assume leftover arguments are files to process.
		process $args;
	fi
}
