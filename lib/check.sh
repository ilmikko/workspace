check_contains() {
	file=$1;
	shift;
	for string in $@; do
		grep -q $file $string || return 1;
	done
}

# Check a .check file.
check() {
	file=$(find_file $1);

	if [ ! -f "$file" ]; then
		error "Cannot find file: $file";
		return 1;
	fi

	script_run "$file" script_run_command "return 1" || return $?;

	return 0;
}
