# Helpers, for now contains functions for easy byte conversion.
# TODO: This probably should contain methods like ends_with and such, abstracted
# away? All these should be available for the install scripts.

humanize_bytes() {
	awk 'function abs(v) { return v < 0 ? -v : v }
	{
		bytes = $1;
		order = log(bytes)/log(2);

		if (order < 10) {
			bytes = bytes;
			unit = "B";
		} else if (order < 20) {
		bytes = (bytes/1024);
		unit = "KiB";
	} else if (order < 30) {
	bytes = (bytes/1048576);
	unit = "MiB";
} else if (order < 40) {
bytes = (bytes/1073741824);
unit = "GiB";
} else {
bytes = (bytes/1099511627776);
unit = "TiB";
}

printf("%.1f"unit, bytes);
}';
}

to_bytes() {
	awk '{
	type = toupper($1);
	bytes = $1;

	sub("^[0-9.]*", "", type);
	sub("[^0-9.]*$", "", bytes);

	switch(type) {
	case "TIB":case "T":
		bytes *= 1099511627776;
		break;
		case "GIB":case "G":
			bytes *= 1073741824;
			break;
			case "MIB":case "M":
				bytes *= 1048576;
				break;
				case "KIB":case "K":
					bytes *= 1024;
					break;
					case "TB":
						bytes *= 1e12;
						break;
						case "GB":
							bytes *= 1e9;
							break;
							case "MB":
								bytes *= 1e6;
								break;
								case "KB":
									bytes *= 1e3;
									break;
									case "B":
										break;
									}
								print(bytes);
							}';
					}
