INTERACTIVE=1;
# Don't be interactive if we are running in a subshell.
[ -t 1 ] || INTERACTIVE=0;

read_bytes() {
	printf "> ";
	read input;
	[ -z "$input" ] && return 2;

	return 0;
}

read_device() {
	printf "> ";
	read input;
	[ -z "$input" ] && return 2;

	if [ ! -b "$input" ]; then
		warn "Device '$input' does not exist!";
		echo;
		return 2;
	fi

	return 0;
}

read_file() {
	printf "> ";
	read input;
	[ -z "$input" ] && return 2;

	if [ ! -f "$input" ]; then
		warn "File '$input' does not exist!";
		echo;
		return 2;
	fi

	return 0;
}

read_number() {
	printf "> ";
	read input;
	[ -z "$input" ] && return 2;

	echo $input | grep -q "^\-\?[0-9]*\(\.[0-9]*\)\?$" || return 2;

	return 0;
}

read_text() {
	printf "> ";
	read input;
	[ -z "$input" ] && return 2;

	return 0;
}
