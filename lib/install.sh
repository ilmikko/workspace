# Install a .install file.
install() {
	path=$1;

	# Dirty replace so that we won't exit in install scripts.
	tmp=$(mktemp);
	sed -e 's/exit/return/g' $path > $tmp;

	. $tmp;
	return $?;
}
