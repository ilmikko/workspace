VERBOSITY=0;
SILENCE=0;

debug() {
	verbose 1 && echo $@;
}

warn() {
	echo [33mwarning:[m $@;
}

error() {
	echo [31merror:[m $@;
}

fail() {
	echo $@ 2>&1;
	exit 1;
}

log() {
	[ "$TABCOUNT" -le "$((SILENCE+VERBOSITY))" ] || return;
	[ "$TABCOUNT" -ge "$SILENCE" ] || return;
	echo "$@" | log_tab_print;
}

TABCOUNT=0;

log_tab_in() {
	TABCOUNT=$((TABCOUNT+1));
}

log_tab_out() {
	TABCOUNT=$((TABCOUNT-1));
}

log_tab_print() {
	awk '{ for (i = 0; i < '$((TABCOUNT-SILENCE))'; i++) printf "  "; print $0 }';
}

increase_silence() {
	SILENCE=$((SILENCE+1));
}

increase_verbosity() {
	VERBOSITY=$((VERBOSITY+1));
}

verbose() {
	limit=$1;
	[ -z "$limit" ] && limit=0;
	[ "$VERBOSITY" -gt $limit ];
}
