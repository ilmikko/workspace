package_display() {
	package_info $1;
	echo;
	package_check $1;
}

package_name() {
	basename $1 .package;
}

package_requires_root() {
	root=$(package_info_file $1 | get_value ROOT);
	[ "$root" = "1" ];
}

package_info_file() {
	folder=$1;
	name=$(package_name $1);

	[ -f "$folder/$name.info" ] || return 1;
	cat "$folder/$name.info";
}

package_info() {
	info=$(package_info_file $1);

	title=$(echo "$info" | get_value TITLE);
	[ $? = 0 ] || title="Unknown Title";
	desc=$(echo "$info" | get_value DESC);
	[ $? = 0 ] || desc="No description";
	args=$(echo "$info" | get_value ARGS);
	[ $? = 0 ] || args="";
	oargs=$(echo "$info" | get_value OARGS);
	[ $? = 0 ] || oargs="";

	name=$(package_name $1);
	log "=- Package $name : $title -=";
	log "$desc";
	package_requires_root $1 && log "[1mRequires root[m" || log "Does not require root";
	if [ -n "$args" ]; then
		log;
		log "Arguments:";
		echo "$args" | awk -F= '{ print $1"\t"$2 }';
	fi
	if [ -n "$oargs" ]; then
		log "Optional arguments:";
		echo "$oargs" | awk -F= '{ print $1"\t"$2 }';
	fi
}

package_check_config() {
	folder=$1;

	if [ ! -d "$folder" ]; then
		error "Not a folder: $folder";
		return 1;
	fi

	name=$(package_name $folder);

	check="$folder/$name.check";
	install="$folder/$name.install";
	recipe="$folder/$name.recipe";

	if [ ! -f "$recipe" ] && [ ! -f "$install" ]; then
		log "[31;1mFAIL[m";
		log "Misconfigured package!";
		log "Cannot find install: $install";
		return 1;
	fi

	return 0;
}

package_check_variables() {
	folder=$1;
	name=$(package_name $folder);

	# Check that the variables are defined.
	# TODO: is this method really needed?
	info="$folder/$name.info";
	define="";
	define_file=$(mktemp);
	[ -f "$info" ] || return 0;
	cat "$info" | get_value ARGS | while read line;\
	do
		key=$(echo $line | cut -d= -f1);
		if ! is_variable $key; then
			error "Variable $key is not defined!";
			return 1;
		fi
	done
}

package_check() {
	package_check_variables $1 || return 1;

	folder=$1;
	name=$(package_name $folder);

	package_check_config $folder || return 1;

	# Nonexistent check file means we are ready.
	if [ ! -f "$check" ]; then
		log "[33mREADY[m";
		return 100;
	fi

	# Check if the package target (check) has been reached.
	check $check;
	EC=$?;

	if [ $EC -gt 0 ]; then
		log "[33mREADY[m";
		return 100;
	else
		log "[32mOK[m";
		return 0;
	fi
}

package_gather_variables() {
	info=$(package_info_file $1);
	if [ "$2" = "--required" ]; then
		package_gather_variables_required;
	elif [ "$2" = "--optional" ]; then
		package_gather_variables_optional;
	else
		package_gather_variables_required;
		package_gather_variables_optional;
	fi

	# Also traverse through subpackages.
	for package in $(package_subpackages $1); do
		file=$(find_file $package);
		[ $? -gt 0 ] && break;
		package_gather_variables $file;
	done
}

package_gather_variables_required() {
	echo "$info" | get_value ARGS | while read line;\
	do
		echo $line;
	done
}
package_gather_variables_optional() {
	echo "$info" | get_value OARGS | while read line;\
	do
		echo $line;
	done
}

package_run_install() {
	log_tab_in;

	install $1;
	EC=$?;

	debug "[ exited with code $EC ]";
	log_tab_out;

	if [ $EC -gt 0 ]; then
		log "[31;1mFAIL[m";
		error "Installation failed!";
		return 1;
	fi
}

package_run_recipe() {
	log_tab_in;

	recipe $1;
	EC=$?;

	debug "[ exited with code $EC ]";
	log_tab_out;

	if [ $EC -gt 0 ]; then
		log "[31;1mFAIL[m";
		error "Recipe failed!";
		return 1;
	fi
}

# List all referenced other packages.
package_subpackages() {
	package_check_config $1 || return;

	local refs="";
	if [ -f "$recipe" ]; then
		refs="$refs $(script_run $recipe script_command_references)";
	fi
	if [ -f "$check" ]; then
		refs="$refs $(script_run $check script_command_references)";
	fi

	# Recursively check through refs.
	# TODO: This has the possibility of going to an infinite loop.
	for ref in $refs; do
		file=$(find_file "$ref" "$1");
		echo $file;
		package_subpackages $file;
	done
}

package_install() {
	folder=$1;

	name=$(package_name $folder);

	if package_requires_root $1 && [ $(workspace_uid) != 0 ]; then
		log "[31;1mFAIL[m";
		error "This package requires root.";
		error "You need root privileges to continue.";
		exit 1;
	fi

	package_check_config $folder || return 1;

	if [ -f "$install" ]; then
		package_run_install "$install" || return 1;
	fi

	if [ -f "$recipe" ]; then
		package_run_recipe "$recipe" || return 1;
	fi

	# If we have a check file, check that the package has been installed
	if [ -f "$check" ]; then
		log_tab_in;

		check $check;
		EC=$?;

		debug "[ exited with code $EC ]";
		log_tab_out;

		if [ $EC -gt 0 ]; then
			log "[31;1mFAIL[m";
			error "Installation after-checks failed!";
			return 1;
		fi
	fi

	log "[32mSUCCESS[m";
	return 0;
}

# Check and install a .package folder.
package() {
	local package_folder=$1
	[ $? -gt 0 ] && fail "Failed to install package: $package_folder does not exist!";

	# Run the checks.
	debug "Check stage: $package_folder";
	log "Checking $package_folder...";
	package_check $package_folder;

	# Return code 100 means everything is OK, but we need to install.
	EC=$?;
	if [ "$EC" = "0" ]; then
		return 0;
	elif [ "$EC" != "100" ]; then
		return 1;
	fi

	# Finally, install.
	debug "Install stage: $package_folder";
	log "Installing $package_folder...";

	package_install $package_folder;
	return $?;
}
