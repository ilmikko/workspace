try_file() {
	if [ -e "$1" ]; then
		echo $1;
		return 0;
	fi

	if [ -d "$1.package" ]; then
		echo $1.package;
		return 0;
	fi

	if [ -f "$1.recipe" ]; then
		echo $1.recipe;
		return 0;
	fi

	if [ -f "$1.check" ]; then
		echo $1.check;
		return 0;
	fi

	if [ -f "$1.install" ]; then
		echo $1.install;
		return 0;
	fi

	return 1;
}

# Find a given file.
find_file() {
	# Consider inside current package.
	# This is to enable packages to override packages recursively.
	for file in "$2/$1" "$1" "$DIR/packages/$1"; do
		try_file "$file" && return 0;
	done

	echo $1;
	return 1;
}

# Get a value for a key-value pair.
get_value() {
	FOUND=0;
	while read line; do
		echo "$line" | grep -q "^$1=" || continue;
		echo "$line" | sed -e "s/^$1=//" | sed -e "s/;$//" | sed -e "s/^\"\|\"$//g";
		FOUND=1;
	done
	[ "$FOUND" = 1 ] && return 0;
	return 1;
}
