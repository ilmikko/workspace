recipe() {
	file=$(find_file $1);

	if [ ! -f "$file" ]; then
		error "Cannot find file: $file";
		return 1;
	fi

	script_run "$file" script_run_command "return 1" || return $?;

	return 0;
}
