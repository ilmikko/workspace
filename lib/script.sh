# We don't want to run bash scripts blindly.
# This is why .check and .recipe files are not executed, but rather parsed using the following rules.
# This allows us to choose which rules we "execute", as well.

# Removes comments from the lines.
remove_comments() {
	awk '{ sub(/\s*#.*$/,"",$0); print $0; }';
}

normalize() {
	awk '{ sub(/;*\s*$/,"",$0); gsub(/^\s*"|"$/,"",$0); print $0; }';
}

get_command() {
	awk '{ print $1 }';
}

get_arguments() {
	awk '{ $1=""; sub(/^\s*/,"",$0); print $0 }';
}

script_split_if() {
	condition="";
	do_then="";
	do_else="";

	# Construct condition.
	while [ $# -gt 0 ]; do
		condition="$condition $1";
		shift;
		[ "$1" = "then" ] && break;
	done
	if [ $# = 0 ]; then
		error "Syntax error: No 'then' keyword!";
		return 1;
	fi
	shift;

	if [ $# = 0 ]; then
		error "Syntax error: Empty 'then' block!";
		return 1;
	fi

	# Construct do_then.
	while [ $# -gt 0 ]; do
		do_then="$do_then $1";
		shift;
		[ "$1" = "else" ] && break;
	done
	shift;

	# Construct do_else.
	while [ $# -gt 0 ]; do
		do_else="$do_else $1";
		shift;
	done
}

script_run_command_contains() {
	file=$1;
	shift;
	for arg in $@; do
		grep -q $arg $file || return 1;
	done
	return 0;
}

script_run_command_if() {
	script_split_if $@;

	if script_run_command $condition; then
		script_run_command $do_then;
	else
		[ -z "$do_else" ] || script_run_command $do_else;
	fi
}

script_run_command() {
	run_cmd=$(echo $@ | get_command | normalize);
	run_args=$(echo $@ | get_arguments | normalize);

	debug "cmd: $run_cmd";
	debug "args: $run_args";

	case $run_cmd in
		!) # Not
			script_run_command $run_args && return 1;
			return 0;
			;;
		?) # Optional
			script_run_command $run_args;
			return 0;
			;;
		check)
			file=$(find_file "$run_args" "$(dirname $script_file)");
			run_as $(get_uid) "check $file" || return 1;
			;;
		command)
			# We cannot use command -v as this also includes shell functions.
			paths=$(echo "$PATH" | awk -F: '{ for (i=0; i<=NF; i++) print $i; }');
			for command in $run_args; do
				FOUND="";
				for dir in $paths; do
					if [ -f "$dir/$command" ]; then
						FOUND=1;
						break;
					fi
				done
				[ ! -z "$FOUND" ] || return 1;
			done
			return 0;
			;;
		connected)
			curl --silent --max-time 5 "$run_args" 1>/dev/null || return 1;
			return 0;
			;;
		contains)
			script_run_command_contains $run_args;
			;;
		echo)
			echo $run_args;
			return 0;
			;;
		fail)
			[ -n "$run_args" ] && echo $run_args;
			return 1;
			;;
		false)
			return 1;
			;;
		file)
			for file in $run_args; do
				[ -f "$file" ] || return 1;
			done
			return 0;
			;;
		folder)
			for folder in $run_args; do
				[ -d "$folder" ] || return 1;
			done
			return 0;
			;;
		if)
			script_run_command_if $run_args;
			;;
		install)
			file=$(find_file "$run_args" "$(dirname $script_file)");
			case $file in
				*.check)
					check $file;
					;;
				*)
					package ${file%.package}.package;
					;;
			esac
			;;
		package)
			file=$(find_file "$run_args" "$(dirname $script_file)");
			run_as $(get_uid) "package $file" || return 1;
			return 0;
			;;
		switch)
			# This requires root access if we are to switch to any other user than ourselves.
			uid=$(echo $run_args | cut -d' ' -f1);
			workspace_uid=$(workspace_uid);
			current_uid=$(get_uid);
			if [ "$uid" != "$current_uid" ]; then
				if [ "$workspace_uid" != 0 ]; then
					error "Root access is required to switch user ids.";
					exit 3;
				fi
				# Check if the user exists.
				id "$uid" || return 1;
				# Switch to the user.
				set_uid "$uid" || return 1;
			fi
			return 0;
			;;
		true)
			return 0;
			;;
		user)
			for user in $run_args; do
				id $user || return 1;
			done
			return 0;
			;;
		variable)
			for var in $run_args; do
				is_variable $var || return 1;
			done
			return 0;
			;;
		*)
			error "Unknown command: $run_cmd";
			exit 2;
			;;
	esac
}

run_lines() {
	local EC=0;
	while read line; do
		[ -n "$line" ] || continue;
		$1 "$line";
		EC=$?;
		[ $EC -gt 0 ] && $2;
	done
	return $EC;
}

script_run() {
	script_file=$1;

	if [ ! -f "$script_file" ]; then
		error "Cannot find file: $script_file";
		return 5;
	fi

	on_fail="$3";
	[ -z "$on_fail" ] && on_fail=true;

	cat $script_file | remove_comments | replace_variables | run_lines "$2" "$on_fail" || return $?;
}
