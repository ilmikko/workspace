script_command_references() {
	run_cmd=$(echo $@ | get_command | normalize);
	run_args=$(echo $@ | get_arguments | normalize);

	case $run_cmd in
		!)
			script_command_references $run_args;
			;;
		check)
			echo $run_args;
			;;
		if)
			script_split_if $@;

			if script_command_references $condition; then
				script_command_references $do_then;
			else
				[ -z "$do_else" ] || script_command_references $do_else;
			fi
			;;
		install)
			echo $run_args;
			;;
		package)
			echo $run_args;
			;;
	esac
}
