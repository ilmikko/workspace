# TODO: Revamp these.
# This is here so it can be overwritten for tests.
workspace_uid() {
	echo $UID;
}

# These are the uids that we use for the recipes, installs and checks.
RUN_AS=$(workspace_uid);

get_uid() {
	echo $RUN_AS;
}
set_uid() {
	RUN_AS=$1;
}

# Retrieve username from user id/name.
get_user() {
	id $1 | grep -o '(\w*)' | grep -o '\w*' -m1;
}

# Run a command as a user.
# Example usage: run_as 1000 whoami
run_as() {
	user=$(get_user $1);
	shift;
	debug "Run command '$@' as $user";
	if [ $(get_uid) = $(workspace_uid) ]; then
		$@;
	else
		su $user --command "$@";
	fi
}
