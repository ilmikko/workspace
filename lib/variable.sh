variable_file=$(mktemp);
define_file=$(mktemp);

replace_variables() {
	filter=$(awk -F= '{ key=$1; $1=""; sub(/^\s*/,"",$0); gsub("/","\\/",$0); printf "%s;","s/{{"key"}}/"$0"/g" }' "$variable_file");
	sed "$filter;s/{{[^}]*}}//g";
}

fill_variables() {
	gather_variables $@ | while read line;\
	do
		key=$(echo $line | variable_key);
		is_variable "$key" && continue;
		needs_definition $line;
	done
	needs_definition_resolve;
}

gather_variables() {
	for file in $@; do
		case $file in
			*.package)
				package_gather_variables $file --required;
				;;
			*)
				;;
		esac
	done
}

get_variable() {
	is_variable $1 || return;
	grep "^$1=" "$variable_file" | tail -n1 | cut -d= -f2-;
}

is_variable() {
	grep -q "^$1=" "$variable_file";
}

needs_definition() {
	key=$(echo $@ | variable_key);
	grep -q "^$key" "$define_file" && return;
	echo "$@" >> "$define_file";
}

needs_definition_resolve() {
	while true; do
		# TODO: make this easier; remove and print at the same time?
		line=$(tail -n1 "$define_file" | remove_comments);
		sed -i '$d' "$define_file";

		[ -z "$line" ] && break;

		key=$(echo $line | variable_key);
		desc=$(echo $line | variable_desc);
		type=$(echo $line | variable_type);
		[ -z "$type" ] && type="text";

		echo "$desc ($type)";

		if [ "$INTERACTIVE" != "1" ]; then
			error "Variable undefined: $key!";
			exit 1;
		fi

		case $type in
			bytes)
				read_bytes;
				EC=$?;
				;;
			device)
				read_device;
				EC=$?;
				;;
			file)
				read_file;
				EC=$?;
				;;
			number)
				read_number;
				EC=$?;
				;;
			text)
				read_text;
				EC=$?;
				;;
			*)
				warn "Unknown variable type: $type";
				warn "Reverting to text.";
				read_text;
				EC=$?;
				;;
		esac

		if [ $EC = 0 ]; then
			set_variable $key $input;
		elif [ $EC = 2 ]; then
			echo "$line" >> "$define_file";
		fi
	done

	rm $define_file;
	log;
}

set_variable() {
	name=$1;
	if ! echo $1 | grep -q "^\w*$"; then
		error "Invalid characters in variable name: $1";
		exit 1;
	fi
	shift;

	# Remove old references to the variable.
	if [ -f "$variable_file" ]; then
		new_file=$(mktemp);
		awk '!/^'"$name"'=/' "$variable_file" > "$new_file";
		mv "$new_file" "$variable_file";
	fi

	echo "$name=$@" >> "$variable_file";
}

variable_key() {
	awk -F= '{ print $1 }';
}

variable_desc() {
	awk -F= '{ print $2 }';
}

variable_type() {
	awk -F= '{ print $3 }';
}
