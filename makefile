all: test

pre-push: test

crush:
	@../crush/cru.sh . --command "echo ./workspace.sh --install workspace"

setup:
	@echo "First time setup..."
	git config core.hooksPath .githooks

unsetup:
	@echo "Removing setup..."
	git config core.hooksPath none

test:
	@./test.sh $(1) $(2)
