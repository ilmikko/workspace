#!/usr/bin/env bash
DIR=$(dirname $0);

PASS=0;
FAIL=0;
SKIP=0;
FAILLOG=$(mktemp /tmp/faillog.XXXXXX);

# Create a temporary runtime directory for the tests.
run=$(mktemp --directory);

# Copy our files over.
# TODO: tests are copied as well at the moment
# TODO: make them immutable so that we won't accidentally remove files, e.g. all tests get the same files
cp -r "$DIR" "$run";

# You can filter tests using this simple trick! Doctors hate them!
filters=$(echo $@ | tr ' ' '|');

echo "[1mTesting for $([ -z "$filters" ] && echo 'all' || echo "'$filters'")...[m";

cd "$run";

for folder in tests/*; do
	[ -z "$filters" ] && printf " ";
	tests="$folder"/*.sh;
	cp -r "$folder/"* "$run";

	for test_template in $tests; do
		# Anything with a 'SKIP' in the path will get skipped.
		if echo "$test_template" | grep -q "SKIP"; then
			[ -z "$filters" ] && printf "[1;33m:[m";
			SKIP=$((SKIP+1));
			continue;
		fi

		# Anything that does not pass the filters will get skipped.
		if [ -z "$(echo "$test_template" | awk "/$filters/")" ]; then
			[ -z "$filters" ] && printf "[1;33m?[m";
			SKIP=$((SKIP+1));
			continue;
		fi

		title="$test_template";
		name=$(basename $test_template);
		test="$run/$name";

		# Boilerplate.
		(
		echo ". $DIR/assert.sh || exit 222";
		echo ". $DIR/lib.sh || exit 223";
		echo "cd $run";
		cat "$test_template" | awk '{ print $0; print "EC=$?; [ $EC == 0 ] || exit $EC;" }';
		) > "$test";
		chmod +x "$test";

		# Don't care about the output unless there is an error.
		[ -z "$filters" ] || printf "%s " "$test_template";

		LOG=$($test 2>&1);
		EC=$?;
		if [ $EC = 0 ]; then
			# Pass.
			[ -z "$filters" ] && printf "[32m.[m" || echo "[32mPASS[m";
			PASS=$((PASS+1));
		else
			# Fail.
			[ -z "$filters" ] && printf "[1;31m![m" || echo "[1;31mFAIL[m";
			FAIL=$((FAIL+1));
			if [ -z "$filters" ]; then
				echo "[m----- $title -----" >> "$FAILLOG";
				echo "$LOG" >> "$FAILLOG";
				echo "[m----- ${title//[0-9A-Za-z\/\-.*]/-} -----" >> "$FAILLOG";
			else
				echo "------------------------------------------------";
				echo "$LOG";
				echo "------------------------------------------------";
				# bash;
			fi
		fi
	done
done

rm -rf "$run";

echo;

echo "Test summary:";
if [ $PASS -gt 0 ]; then
	echo "[1;32m  Passes: $PASS[m";
else
	echo "  Passes: 0";
fi
if [ $FAIL -gt 0 ]; then
	echo "[1;31mFailures: $FAIL[m";
else
	echo "Failures: 0";
fi
if [ $SKIP -gt 0 ]; then
	echo "[1;33m   Skips: $SKIP[m";
fi

echo;

if [ $FAIL -gt 0 ]; then
	if [ -z "$filters" ]; then
		echo "[1;31mFailure log:[m";
		cat "$FAILLOG";
		echo;
	fi
	rm "$FAILLOG";
	echo "[31mFailures during testing.[m";
	exit $FAIL;
else
	echo "[32mAll tests pass :D[m";
	rm "$FAILLOG";
fi
