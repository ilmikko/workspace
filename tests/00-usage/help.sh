assert_success "./workspace.sh --help";
assert_exec_find "./workspace.sh --help" "Usage" "Commands";
assert_exec_find "./workspace.sh --help" "help"; # Assert that we see even some commands.
# Assert that each advertised command is found.
cmds=$(./workspace.sh --help | awk '{ if (cmds) print $1 } /Commands/ { cmds=1; }');
for cmd in $cmds; do
	if assert_exec_find "./workspace.sh $cmd" "Unknown command" >/dev/null 2>&1; then
		echo "Command $cmd is unknown, even when advertised by --help!";
		echo $(./workspace.sh $cmd);
		exit 1;
	fi
done
