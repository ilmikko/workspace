assert_equals "$(echo "#comment" | remove_comments)" "";
assert_equals "$(echo "# comment" | remove_comments)" "";
assert_equals "$(echo "something      # comment" | remove_comments)" "something";
assert_equals "$(echo "something #comment" | remove_comments)" "something";
assert_equals "$(echo "something#comment" | remove_comments)" "something";
