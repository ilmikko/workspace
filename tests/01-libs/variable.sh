assert_success "set_variable test1 10";
assert_success "set_variable test2 20";
assert_success "set_variable test3 30";
assert_success "is_variable test1";
assert_fail "is_variable test4";
assert_exec_equals "get_variable test2" "20";
assert_exec_equals "get_variable test3" "30";

# Invalid characters.
assert_fail "set_variable someth/ing 10";
assert_fail "set_variable abc#ed 10";
