# Test that variables that are considered 'global' get removed from the variable list.
# For example, $0 --install --verbose something.package isn't going to try install '--verbose'.
assert_success "./workspace.sh --install success.package";
assert_success "./workspace.sh --install --verbose success.package";
