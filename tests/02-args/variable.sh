# Assert that variables can be passed after regular arguments.
assert_fail "./workspace.sh test.install";
assert_success "./workspace.sh VARIABLE=var test.install";
assert_success "./workspace.sh test.install VARIABLE=var";
