assert_exec_equals "find_file find_file.sh" "find_file.sh";
assert_exec_equals "find_file some_directory/some.package" "some_directory/some.package";
assert_exec_equals "find_file some_directory/some" "some_directory/some.package";
assert_exec_equals "find_file some" "some.package";
assert_exec_equals "find_file some nonexistent_directory" "some.package";

assert_exec_equals "find_file some some_directory" "some_directory/some.package";
assert_exec_equals "find_file body some_directory" "some_directory/body.recipe";
assert_exec_equals "find_file once some_directory" "some_directory/once.check";
assert_exec_equals "find_file told some_directory" "some_directory/told.install";
assert_exec_equals "find_file me some_directory" "me";

# Change of current directory shouldn't affect results, as files inside packages take precedence.
assert_exec_equals "find_file some some_directory/other_directory" "some_directory/other_directory/some.check";
cd some_directory;
assert_exec_equals "find_file some other_directory" "other_directory/some.check";
# However, if it is not found inside the package, we check the current directory.
assert_exec_equals "find_file once other_directory" "once.check";
assert_exec_equals "find_file told other_directory" "told.install";
