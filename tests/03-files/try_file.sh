# File should be left alone if it's found.
assert_exec_equals "try_file some_directory/some.check" "some_directory/some.check";
# It should succeed if it's found.
assert_success "try_file some_directory/some.check";
# We should return empty if the file doesn't exist.
assert_exec_equals "try_file some_directory/nonexistent.file" "";
# We should also return an error code.
assert_fail "try_file some_directory/nonexistent.file";
