# We shouldn't convert to .package if .package is a file.
assert_exec_equals "try_file some_directory/me" "";
assert_fail "try_file some_directory/me";
