# We should try files in the right order.
# Packages first.
assert_exec_equals "try_file some_directory/some" "some_directory/some.package";
# Then recipes.
assert_exec_equals "try_file some_directory/body" "some_directory/body.recipe";
# Then checks.
assert_exec_equals "try_file some_directory/once" "some_directory/once.check";
# Then finally installs.
assert_exec_equals "try_file some_directory/told" "some_directory/told.install";
