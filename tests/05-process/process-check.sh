assert_exec "./workspace.sh file.unknown" 1;
assert_exec_find "./workspace.sh file.unknown" "Unknown file" "file.unknown";
assert_exec "./workspace.sh nonexistent.unknown" 1;
assert_exec_find "./workspace.sh nonexistent.unknown" "File not found" "nonexistent.unknown";
assert_exec "./workspace.sh nonexistent.check" 1;
assert_exec_find "./workspace.sh nonexistent.check" "File not found" "nonexistent.check";
