set_variable VARIABLE My
set_variable ANOTHER_VARIABLE favourite
assert_equals "$(cat variable_file.txt | replace_variables)" "$(printf "My favourite path is .\n  is ")";
set_variable NAME Albert
set_variable SUPERSTAR "a superstar"
set_variable END !
assert_equals "$(cat variable_file.txt | replace_variables)" "$(printf "My favourite path is .\nAlbert  is a superstar!")";
set_variable PATH /dev/sdb
assert_equals "$(cat variable_file.txt | replace_variables)" "$(printf "My favourite path is /dev/sdb.\nAlbert  is a superstar!")";
