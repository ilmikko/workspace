# Here we want to assert that we cannot in fact inject bad stuff into our checks.

# The first ones should fail because of unknown commands.
assert_exec "./workspace.sh line.check" 1;
assert_exec "./workspace.sh subshell.check" 1;
# The third one should succeed because the text is just echoed.
assert_exec "./workspace.sh or.check" 0;
