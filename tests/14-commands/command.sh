assert_success "script_run_command command awk";
assert_success "script_run_command command [";
assert_fail "script_run_command command exec_check_cmd";
assert_fail "script_run_command command ./workspace.sh";
assert_fail "script_run_command command nonexistent";
assert_fail "script_run_command command uninstalled_command";
