assert_exec_equals "script_run_command echo hello world" "hello world";
assert_exec_equals "script_run_command echo hello world!" "hello world!";
assert_exec_equals "script_run_command echo hello world;" "hello world";
assert_exec_equals "script_run_command echo hello; world" "hello; world";
assert_exec_equals "script_run_command echo \"hello world\"" "hello world";
assert_exec_equals "script_run_command echo \"hello world\";" "hello world";
