assert_success "script_run_command file ./file.sh";
assert_success "script_run_command file file.sh";
assert_success "script_run_command file ././././file.sh";
assert_fail "script_run_command file ../file.sh";
assert_fail "script_run_command file /file.sh";
assert_fail "script_run_command file nonexistent.sh";
# Some random injections just in case
assert_fail "script_run_command file \"] && exit 22 && [\"";
assert_fail "script_run_command file \";;;;[][]][][]^&^&^^&&^";
