assert_success "script_run_command folder ./";
assert_success "script_run_command folder .";
assert_success "script_run_command folder ././././";
assert_success "script_run_command folder ../";
assert_success "script_run_command folder /";
assert_fail "script_run_command folder ./nonexistent";
assert_fail "script_run_command folder nonexistent";
