assert_success "script_run_command if true then true";
assert_fail "script_run_command if true then false";
assert_success "script_run_command if false then true";
assert_success "script_run_command if false then false";

assert_success "script_run_command if true then true else true";
assert_success "script_run_command if false then true else true";
assert_fail "script_run_command if true then false else true";
assert_success "script_run_command if false then false else true";
assert_success "script_run_command if true then true else false";
assert_fail "script_run_command if false then true else false";
assert_fail "script_run_command if true then false else false";
assert_fail "script_run_command if false then false else false";

assert_fail "script_run_command if true";
assert_fail "script_run_command if true then";
