assert_fail "script_run_command ! true";
assert_success "script_run_command ! false";
# 'not' does not hide command errors.
assert_fail "script_run_command ! nonexistent";
