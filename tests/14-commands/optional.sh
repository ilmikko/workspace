assert_fail "script_run_command fail"
assert_success "script_run_command ? fail";
assert_success "script_run_command ? check nonexistent";
# Nonexistent commands should still fall through.
assert_fail "script_run_command ? nonexistent";
