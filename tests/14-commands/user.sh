assert_success "script_run_command user root";
assert_success "script_run_command user 0";
assert_fail "script_run_command user nonexistent_user";
assert_fail "script_run_command user 9999999";
