assert_success "script_run_command contains somefile.txt one";
assert_success "script_run_command contains somefile.txt two";
assert_fail "script_run_command contains somefile.txt three";
assert_success "script_run_command contains somefile.txt our";
assert_fail "script_run_command contains somefile.txt iv";

assert_success "script_run_command contains somefile.txt f o u r";
assert_fail "script_run_command contains somefile.txt f x y z";

# Regex-like forms are supported, a bit like grep.
assert_success "script_run_command contains somefile.txt ne$";
assert_fail "script_run_command contains somefile.txt ^our";
