set_variable "VARIABLE" "defined";
assert_equals "$(script_run "var_script.dsh" script_run_command)" "The variable is defined!";

set_variable "DEFINED_OR_NOT_DEFINED" "not defined";
set_variable "IS_VAR" "is not";
set_variable "VARIABLE" "variable";
set_variable "THE" "The";

assert_equals "$(script_run "multivar_script.dsh" script_run_command)" "The variable is not not defined!";
