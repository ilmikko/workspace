assert_success "./workspace.sh success.install";
assert_fail "./workspace.sh failing.install";
assert_fail "./workspace.sh implicit-failing.install";
assert_success "./workspace.sh implicit-override.install";
