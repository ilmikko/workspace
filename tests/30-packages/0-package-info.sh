# Usually we don't want to install the package unless explicitly specified.
assert_exec_find_neg "./workspace.sh wim.package" "Installing";
assert_exec_find "./workspace.sh wim.package" "Whale Image Manipulator" "Package wim" "Does not require root" "READY";
