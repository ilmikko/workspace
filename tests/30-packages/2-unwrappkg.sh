# Here we investigate unwrapping a package with common properties, called "whale image manipulator", or wim by short.
rm -rf "./wim-lib" "./wim-bin" || true;
assert_exec "./workspace.sh --install wim.package";
assert_folder "./wim-lib" "./wim-lib/wim";
assert_file "./wim-lib/wim/libfile";
assert_folder "./wim-bin";
assert_file "./wim-bin/wim";
