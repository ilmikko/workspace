# Assert different kinds of failures for packages, as this is a user-facing system.
assert_fail "package_install nonexistent.package"; # folder does not exist
assert_fail "package_install file.package"; # package is not a folder
assert_exec_find "package_install file.package" "Not a folder" "file.package";
assert_fail "package_install noinst.package"; # no install in package
assert_exec_find "package_install noinst.package" "Misconfigured package" "Cannot find install" "noinst.install";
assert_fail "package_install failingcheck.package"; # check is flaky, fails always
assert_exec_find "package_install failingcheck.package" "checks failed";
assert_fail "package_install failinginst.package"; # install fails
assert_exec_find "package_install failinginst.package" "Installation failed";
# Failing commands shouldn't count as a failing check, they should abort the install completely.
assert_fail "package_install failcmd.package";
assert_exec_find "package_install failcmd.package" "Unknown" "command" "nonexistent";
