# A package tree should run all the packages in the tree.
assert_success "./workspace.sh --install install.package PACKAGE=other.package";

# A package that installs a nonexistent package should fail.
assert_fail "./workspace.sh --install install.package PACKAGE=nonexistent.package";

assert_success "./workspace.sh --install recursive.package";
