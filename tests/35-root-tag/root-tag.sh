assert_success "main --install ordinary";

assert_fail "main --install rootreq";
# Simulate root for this test.
workspace_uid() {
	echo 0;
}
assert_success "main --install rootreq";
