# You can install .installs, or entire packages.
assert_success "exec_recipe_cmd install success.install";
assert_success "exec_recipe_cmd install success.package";
# Defaults to .package if nothing is defined.
assert_success "exec_recipe_cmd install success";
assert_fail "exec_recipe_cmd install nonexistent";
