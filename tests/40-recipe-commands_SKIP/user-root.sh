# Simulate behaviour when we are switching users.
echo 0 > "$0.test_user";
get_uid() {
	cat "$0.test_user";
}
set_uid() {
	echo $1 > "$0.test_user";
}
# Let's pretend we are running as root.
workspace_uid() {
	echo 0;
}
run_as() {
	set_uid $1;
	shift;
	$@;
}

assert_uid_propagation() {
	# Check that we succeed.
	assert_success "exec_recipe_cmd user $1";
	# Check that the command did what it was supposed to.
	assert_equals "$(get_uid)" "$1";
	# Check that the uid gets propagated to the installation scripts.
	mkdir -p ./user.package;
	echo "file $1.installed" > ./user.package/user.check;
	echo "echo UID is \$(get_uid)" > ./user.package/user.install;
	echo "touch \$(get_uid).installed" >> ./user.package/user.install;
	echo "User $1";
	assert_success "exec_recipe_cmd install user";
	echo "User $1 OK";
	rm $1.installed;
}

# Simulate root.
assert_uid_propagation 0;
# Simulate a switch to the existing user.
assert_uid_propagation $UID;
# Simulate root again, making sure the switch is not permanent.
assert_uid_propagation 0;
