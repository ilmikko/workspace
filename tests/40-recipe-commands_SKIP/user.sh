# Existing user should succeed
uid=$UID;
assert_success "exec_recipe_cmd user $uid";
mkdir -p ./user.package;
echo "file $uid.installed" > ./user.package/user.check;
echo "touch \$(get_uid).installed" > ./user.package/user.install;
assert_success "exec_recipe_cmd install user";
exec_recipe_cmd install user;
rm $uid.installed;

# Root user should fail as we are not root.
assert_fail "exec_recipe_cmd user 0";
# Nonexistent user should fail.
assert_fail "exec_recipe_cmd user 999990";
