assert_fail "exec_recipe_cmd install var";
assert_exec_find "exec_recipe_cmd install var" "Undefined variable" "ROOT";

assert_success "exec_recipe_cmd define VARIABLE 20";
assert_success "exec_recipe_cmd define ROOT ./directory";

assert_exec_equals "get_variable VARIABLE" "20";
assert_exec_equals "get_variable ROOT" "./directory";

assert_success "exec_recipe_cmd install var";
