assert_exec_equals "./workspace.sh twosuccess.recipe" "$(printf "YES\nYES")";
assert_exec_equals "./workspace.sh twofail.recipe" "$(printf "NO\nNO")";
assert_exec_equals "./workspace.sh successfail.recipe" "$(printf "YES\nNO")";
assert_exec_equals "./workspace.sh failsuccess.recipe" "$(printf "NO\nYES")";

assert_success "./workspace.sh twosuccess.recipe";
assert_success "./workspace.sh twofail.recipe";
assert_success "./workspace.sh successfail.recipe";
assert_success "./workspace.sh failsuccess.recipe";
