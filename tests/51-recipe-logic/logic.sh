assert_success "check success.check";
assert_fail "check fail.check";

assert_exec_equals "./workspace.sh onetwo.recipe" "$(printf "ONE!\nTWO!")";
assert_exec_equals "./workspace.sh trueandone.recipe" "$(printf "SUCCESS!\nONE!")";
assert_exec_equals "./workspace.sh trueorone.recipe" "$(printf "SUCCESS!")";
assert_exec_equals "./workspace.sh falseandone.recipe" "$(printf "FAIL!")";
assert_exec_equals "./workspace.sh falseorone.recipe" "$(printf "FAIL!\nONE!")";
