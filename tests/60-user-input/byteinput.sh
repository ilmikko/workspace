# Text input allowing only valid byte sequences.
# Following the convention of fdisk (as we are using fdisk)
# KiB = 1024B, KB = 1000B.
assert_equals "$(printf "%s\n0" "1B" | ./workspace.sh --install byteinput --silent --interactive)" "$(printf "How many bytes in 2.2MiB? (bytes)\n> You chose 1.0B.\nYour answer was 2.2MiB off.")";
assert_equals "$(printf "%s\n0" "2.2B" | ./workspace.sh --install byteinput --silent --interactive)" "$(printf "How many bytes in 2.2MiB? (bytes)\n> You chose 2.2B.\nYour answer was 2.2MiB off.")";
assert_equals "$(printf "%s\n0" "1024B" | ./workspace.sh --install byteinput --silent --interactive)" "$(printf "How many bytes in 2.2MiB? (bytes)\n> You chose 1.0KiB.\nYour answer was 2.2MiB off.")";
assert_equals "$(printf "%s\n0" "1000B" | ./workspace.sh --install byteinput --silent --interactive)" "$(printf "How many bytes in 2.2MiB? (bytes)\n> You chose 1000.0B.\nYour answer was 2.2MiB off.")";

assert_equals "$(printf "%s\n0" "2MB" | ./workspace.sh --install byteinput --silent --interactive)" "$(printf "How many bytes in 2.2MiB? (bytes)\n> You chose 1.9MiB.\nYour answer was 299.7KiB off.")";
assert_equals "$(printf "%s\n0" "2MiB" | ./workspace.sh --install byteinput --silent --interactive)" "$(printf "How many bytes in 2.2MiB? (bytes)\n> You chose 2.0MiB.\nYour answer was 204.8KiB off.")";
assert_equals "$(printf "%s\n0" "2.2MiB" | ./workspace.sh --install byteinput --silent --interactive)" "$(printf "How many bytes in 2.2MiB? (bytes)\n> You chose 2.2MiB.\nYour answer was 0.0B off.")";

assert_equals "$(printf "%s\n0" "200GiB" | ./workspace.sh --install byteinput --silent --interactive)" "$(printf "How many bytes in 2.2MiB? (bytes)\n> You chose 200.0GiB.\nYour answer was -0.2TiB off.")";
