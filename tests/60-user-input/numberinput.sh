# Text input allowing only numbers.
# TODO: Maybe more useful would be byteinput? For things such as 22MiB or 22MB
# or like?
assert_equals "$(printf "%s\n0" "25" | ./workspace.sh --install numberinput --silent --interactive)" "$(printf "Type a number. (number)\n> 25 + 25 = 50")";
#assert_equals "$(printf "not a number\nnan\n0" | ./workspace.sh --install numberinput --silent --interactive)" "$(printf "Type a number. (number)\n> Type a number. (number)\n> Type a number. (number)\n> 0 + 25 = 25")";
assert_equals "$(printf "%s\n0" "-1000" | ./workspace.sh --install numberinput --silent --interactive)" "$(printf "Type a number. (number)\n> -1000 + 25 = -975")";
assert_equals "$(printf "%s\n0" "50.42" | ./workspace.sh --install numberinput --silent --interactive)" "$(printf "Type a number. (number)\n> 50.42 + 25 = 75.42")";
assert_equals "$(printf "%s\n0" "-3.1415926535" | ./workspace.sh --install numberinput --silent --interactive)" "$(printf "Type a number. (number)\n> -3.1415926535 + 25 = 21.8584")";
assert_equals "$(printf "%s\n%s\n%s\n0" "Aerial80" "50%" "75.0" | ./workspace.sh --install numberinput --silent --interactive)" "$(printf "Type a number. (number)\n> Type a number. (number)\n> Type a number. (number)\n> 75.0 + 25 = 100")";
