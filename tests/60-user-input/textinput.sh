# Regular text input.
assert_equals "$(echo test | ./workspace.sh --install textinput --silent --interactive)" "$(printf "User input for testing. (text)\n> User input was test")";
assert_equals "$(echo textinput | ./workspace.sh --install textinput --silent --interactive)" "$(printf "User input for testing. (text)\n> User input was textinput")";
assert_equals "$(printf "one line\ntwo line" | ./workspace.sh --install textinput --silent --interactive)" "$(printf "User input for testing. (text)\n> User input was one line")";
assert_equals "$(printf "\n\none line\ntwo line" | ./workspace.sh --install textinput --silent --interactive)" "$(printf "User input for testing. (text)\n> User input for testing. (text)\n> User input for testing. (text)\n> User input was one line")";
