# Inputs that are not known should always revert to text.
assert_equals "$(printf "4\n0" | ./workspace.sh --install unknowninput --silent --interactive)" "$(printf "Type a chair. (chair)\n[33mwarning:[m Unknown variable type: chair\n[33mwarning:[m Reverting to text.\n> The chair of your choosing was 4!")";
