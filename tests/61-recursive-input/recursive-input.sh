# Test that we get all the arguments needed by packages we might install.
assert_equals "$(printf "This is some text!\n0\n" | ./workspace.sh --install recursive-input --interactive --silent --silent --silent)" "$(printf "Text input. (text)\n> This is some text!")";
