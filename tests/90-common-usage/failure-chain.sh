# Test that a failure will immediately exit, even if the arguments have not been fully parsed yet.
assert_fail "./workspace.sh --install succeeding --install failing FILE=testfile1";
assert_file "testfile1";

assert_fail "./workspace.sh --install failing --install succeeding FILE=testfile2";
assert_file_neg "testfile2";
