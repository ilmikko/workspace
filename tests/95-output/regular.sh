# Check regular output.
assert_exec_equals "./workspace.sh" "Usage: ./workspace.sh [target]";
assert_exec_equals "./workspace.sh nonexistent.package" "File not found: nonexistent.package";
assert_exec_equals "./workspace.sh --install output-success.package" "$(printf "\nChecking output-success.package...\n[33mREADY[m\nInstalling output-success.package...\n[32mSUCCESS[m")";
assert_exec_equals "./workspace.sh --install output-success.package --silent" "$(printf "Success!!")";
