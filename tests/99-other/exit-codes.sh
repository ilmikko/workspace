# This is to check the consistency (and keep track of) of the exit codes.
assert_exec "./workspace.sh --help" 0; # correct usage
assert_exec "./workspace.sh" 1; # usage text
assert_exec "./workspace.sh nonexistent.file" 1; # nonexistent file
assert_exec "./workspace.sh --nonexistent-cmd" 2; # unknown command
assert_exec "./workspace.sh unknown-cmds.check" 1; # unknown workspace command
assert_exec "check success.check" 0; # workspace success
assert_exec "./workspace.sh failing.check" 1; # workspace failure
