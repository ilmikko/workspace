#!/usr/bin/env bash
DIR=$(dirname $0);

. "$DIR"/lib.sh;

if [ $# = 0 ]; then
	usage;
	exit 1;
fi

main $@;
